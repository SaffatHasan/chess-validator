[![pipeline status](https://gitlab.com/saffathasan/chess-validator/badges/master/pipeline.svg)](https://gitlab.com/saffathasan/chess-validator/commits/master)
[![coverage report](https://gitlab.com/saffathasan/chess-validator/badges/master/coverage.svg)](https://gitlab.com/saffathasan/chess-validator/commits/master)
# SE 576
# Chess Move Validator
## Author: Saffat Hasan

# Overview
This project contains the SE 576 chess validation program.

As a general overview, I performed all development on a dev branch on gitlab and used the
gitlab-ci to automate running my tests and lint on a pipeline.

I placed a restriction such that I cannot merge to master unless the following conditions are met:

    - All tests are passing
    - Linting is succesful with 10/10 rating
    
The rubric specifies "Example of Code Improvement w/ Report" for which I will attach a chart
of the Pipeline jobs. As the standard was set very early on to 10/10 to enable merges to master,
the quality has been locked at 10/10.

# Viewing Test Reports
Please refer to htmlcov/index.html
I've also added some meta information about the pipeline itself in meta-statistics/*.pdf

# Thoughts
The automated tests were heavily useful for the refactoring step of TDD. I could confidently touch
any segment of code without worrying about breaking my build.
It was quite difficult to get setup at first and I will need more practice refactoring.

I'm disappointed in the somewhat explosive structure of my code. While each class/file is relatively small,
the interactions between the classes are non-trivial.

In the agile mindset, there isn't a driving need for me to refactor into something like Decorator pattern in which
I layer the services/engines on top of each other and simply do BoardEngine.get_moves() or something along those lines.



# How to Run/Build/Test/Report
There are few interesting targets in the Makefile that will enable you to run.

## Make run 
This will run the project and ask the user for input

## Make sample-run
This will use the sample given by the assignment as input

## Make lint
This will run autopep8 and pylint

## Make test
This will run pytest on the project

## Make coverage
This will run the coverage on the command line 

## Make coverage-html
This will run the coverage reports and generate HTML output.
The output is in htmlcov/