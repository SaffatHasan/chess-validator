.PHONY: all lint test coverage coverage-html run

all: test lint

autopep8:
	autopep8 --in-place --recursive -aaaaa src/

lint: autopep8
	pylint src

test:
	pytest

coverage:
	pytest --cov-report term-missing --cov=src src/test/

coverage-html:
	pytest --cov=src --cov-report html --cov-branch

run:
	PYTHONPATH="${PWD}" python src/main/python/driver.py

run-sample:
	PYTHONPATH="${PWD}" python src/main/python/driver.py < src/test/resources/input/assignment_sample_game.txt
