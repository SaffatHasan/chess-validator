""" Test module for piece_state """
# pylint: disable=R0903
# pylint: disable=C0111
import unittest

from src.main.python.state.piece_enums import Color, PieceTypes
from src.main.python.state.piece_state import King, Piece
from src.main.python.exceptions.board_exceptions import InvalidPieceException


class BoardTest(unittest.TestCase):
    """ Test class for Board """

    def test_piece_representation(self):
        """ Test that a White King is represented by "W" "K" """
        piece = King(Color.WHITE)
        actual = piece.__repr__()
        expected = "WK"

        self.assertEqual(expected, actual)

    def test_raise_invalid_piece_error_when_specifying_invalid_piece(self):
        self.assertRaises(InvalidPieceException, Piece, "Foo", Color.WHITE)

    def test_raise_invalid_piece_error_when_specifying_invalid_color(self):
        self.assertRaises(InvalidPieceException, Piece, PieceTypes.KING, "Bar")
