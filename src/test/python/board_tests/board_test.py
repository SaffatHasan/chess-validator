""" Test module for UserInput """
# pylint: disable=R0903
# pylint: disable=C0111
import unittest
from src.main.python.state.position_state import Coordinates
from src.main.python.state.board_state import Board
from src.main.python.motion.board_movement import move_piece_on_board
from src.main.python.state.piece_state import Piece
from src.main.python.state.piece_enums import PieceTypes, Color
from src.main.python.exceptions.board_exceptions import InvalidPieceException


class BoardTest(unittest.TestCase):
    """ Test class for Board interactions """

    def test_empty_board_returns_none_for_a1(self):
        board = Board()
        actual = board.get_piece_at_coordinate(Coordinates("a1"))
        self.assertIsNone(actual)

    def test_add_white_piece_to_board(self):
        """ Want to test the ability to place a piece onto a specific position on a board """
        rook = Piece(PieceTypes.ROOK, Color.WHITE)
        position = Coordinates("f1")
        board = Board()

        board.add_piece_to_board(rook, position)
        expected = rook
        actual = board.get_piece_at_coordinate(position)
        self.assertEqual(actual, expected)

    def test_rooks_are_equal(self):
        rook1 = Piece(PieceTypes.ROOK, Color.WHITE)
        rook2 = Piece(PieceTypes.ROOK, Color.WHITE)
        self.assertEqual(rook1, rook2)

    def test_rooks_of_different_colors_are_not_equal(self):
        rook1 = Piece(PieceTypes.ROOK, Color.WHITE)
        rook2 = Piece(PieceTypes.ROOK, Color.BLACK)
        self.assertNotEqual(rook1, rook2)

    def test_rook_and_pawn_are_not_equal(self):
        rook = Piece(PieceTypes.ROOK, Color.WHITE)
        pawn = Piece(PieceTypes.PAWN, Color .WHITE)
        self.assertNotEqual(rook, pawn)

    def test_raise_exception_when_using_non_piece_string_on_initialization(
            self):
        self.assertRaises(InvalidPieceException, Piece, "foo", Color.WHITE)

    def test_moving_piece_to_different_coordinate(self):
        board = Board()
        piece = Piece(PieceTypes.KNIGHT, Color.WHITE)
        coordinates_a = Coordinates("a1")
        board.add_piece_to_board(piece, coordinates_a)

        coordinates_b = Coordinates("h7")
        resulting_board = move_piece_on_board(
            board, coordinates_a, coordinates_b)

        actual = resulting_board.get_board_as_dict()
        expected = {coordinates_b: piece}

        self.assertEqual(expected, actual)

    def test_moving_piece_to_different_coordinate_does_not_affect_original_board(
            self):
        board = Board()
        piece = Piece(PieceTypes.KNIGHT, Color.WHITE)
        coordinates_a = Coordinates("a1")
        board.add_piece_to_board(piece, coordinates_a)
        expected = board.get_board_as_dict()

        coordinates_b = Coordinates("h7")
        move_piece_on_board(board, coordinates_a, coordinates_b)

        actual = board.get_board_as_dict()

        self.assertEqual(expected, actual)
