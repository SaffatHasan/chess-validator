""" Test module for adding pieces to the board """
# pylint: disable=C0111

import unittest
from src.main.python.state.position_state import Coordinates
from src.test.python.test_util import get_board_moves


class ValidateRookMovementTest(unittest.TestCase):
    """ Test class for Rook Movement """

    def test_white_rook_moves_forward_one(self):
        test_piece = "Ra1"
        actual = get_board_moves(test_piece, "", test_piece)
        expected = Coordinates("a2")

        self.assertIn(expected, actual)

    def test_white_rook_moves_left_one(self):
        test_piece = "Re4"
        actual = get_board_moves(test_piece, "", test_piece)
        expected = Coordinates("d4")

        self.assertIn(expected, actual)

    def test_white_rook_moves_left_two(self):
        test_piece = "Re4"
        actual = get_board_moves(test_piece, "", test_piece)
        expected = Coordinates("c4")

        self.assertIn(expected, actual)

    def test_white_rook_does_not_move_off_board(self):
        test_piece = "Ra1"
        actual = get_board_moves(test_piece + ", Qb1, Qa2", "", test_piece)
        expected = set()

        self.assertEqual(expected, actual)

    def test_white_rook_eat_black_piece(self):
        test_piece = "Ra1"
        actual = get_board_moves(test_piece + ", Qb1", "Qa2", test_piece)
        expected = Coordinates("a2")

        self.assertIn(expected, actual)

    def test_white_rook_eat_black_piece_two_tiles_away(self):
        test_piece = "Ra1"
        actual = get_board_moves(test_piece + ", Qb1", "Qa3", test_piece)
        expected = Coordinates("a3")

        self.assertIn(expected, actual)
