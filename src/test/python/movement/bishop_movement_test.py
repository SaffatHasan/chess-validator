""" Test module for adding pieces to the board """
# pylint: disable=C0111

import unittest
from src.main.python.state.position_state import Coordinates
from src.test.python.test_util import get_board_moves


class ValidateBishopMovementTest(unittest.TestCase):
    """ Test class for Bishop Movement """

    def test_bishop_can_move_diagonal_one(self):
        test_piece = "Bf3"

        actual = get_board_moves(test_piece, "", test_piece)
        expected = [
            Coordinates("e4"),
            Coordinates("g4"),
            Coordinates("e2"),
            Coordinates("g2")
        ]

        for coordinate in expected:
            self.assertIn(coordinate, actual)

    def test_bishop_can_move_diagonal_two(self):
        test_piece = "Bf3"

        actual = get_board_moves(test_piece, "", test_piece)
        expected = [
            Coordinates("d5"),
            Coordinates("h5"),
            Coordinates("d1"),
            Coordinates("h1")
        ]

        for coordinate in expected:
            self.assertIn(coordinate, actual)
