""" Test module for adding pieces to the board """
# pylint: disable=C0111

import unittest
from src.main.python.state.position_state import Coordinates
from src.test.python.test_util import get_board_moves


class ValidateKingMovementTest(unittest.TestCase):
    """ Test class for King Movement """

    def test_white_king_can_move_f3_forward_to_f4(self):
        test_piece = "Kf3"

        actual = get_board_moves(test_piece, "", test_piece)
        expected = Coordinates("f4")
        self.assertIn(expected, actual)

    def test_white_king_can_move_f4_forward_to_f5(self):
        test_piece = "Kf4"

        actual = get_board_moves(test_piece, "", test_piece)
        expected = Coordinates("f5")
        self.assertIn(expected, actual)

    def test_white_king_can_move_f3_backward_to_f2(self):
        test_piece = "Kf3"

        actual = get_board_moves(test_piece, "", test_piece)
        expected = Coordinates("f2")

        self.assertIn(expected, actual)

    def test_white_king_can_move_e2_left_to_d2(self):
        test_piece = "Ke2"

        actual = get_board_moves(test_piece, "", test_piece)
        expected = Coordinates("d2")

        self.assertIn(expected, actual)

    def test_black_king_can_move_d6_right_to_c6(self):
        test_piece = "Kd6"

        actual = get_board_moves("", test_piece, test_piece)
        expected = Coordinates("c6")

        self.assertIn(expected, actual)

    def test_white_king_can_not_move_forward_into_friendly(self):
        test_piece = "Ke2"
        white_pieces = test_piece + ", Pe3"

        actual = get_board_moves(white_pieces, "", test_piece)
        not_expected = Coordinates("e3")

        self.assertNotIn(not_expected, actual)

    def test_king_can_move_diagonal(self):
        test_piece = "Kf3"

        actual = get_board_moves(test_piece, "", test_piece)
        expected = {
            Coordinates("e2"),
            Coordinates("g4"),
            Coordinates("e4"),
            Coordinates("g2")
        }

        self.assertTrue(expected.issubset(actual))

    def test_all_king_movements_without_piece_restrictions(self):
        test_piece = "Kf3"

        actual = get_board_moves(test_piece, "", test_piece)
        expected = {
            Coordinates("g4"),
            Coordinates("e4"),
            Coordinates("e2"),
            Coordinates("g2"),
            Coordinates("f4"),
            Coordinates("f2"),
            Coordinates("e3"),
            Coordinates("g3"),
        }

        self.assertEqual(expected, actual)
