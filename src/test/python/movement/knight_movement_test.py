""" Test module for adding pieces to the board """
# pylint: disable=C0111

import unittest
from src.main.python.state.position_state import Coordinates
from src.test.python.test_util import get_board_moves


class ValidateKnightMovementTest(unittest.TestCase):
    """ Test class for Knight Movement """

    def test_white_knight_unrestricted(self):
        """ Should see a total of 8 moves """
        test_piece = "Nf3"

        actual = get_board_moves(test_piece, "", test_piece)
        expected = {
            Coordinates("h2"),
            Coordinates("g5"),
            Coordinates("d2"),
            Coordinates("e1"),
            Coordinates("h4"),
            Coordinates("e5"),
            Coordinates("d4"),
            Coordinates("g1")
        }

        self.assertEqual(expected, actual)
