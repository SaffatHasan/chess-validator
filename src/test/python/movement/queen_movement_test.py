""" Test module for adding pieces to the board """
# pylint: disable=C0111

import unittest
from src.main.python.state.position_state import Coordinates
from src.test.python.test_util import get_board_moves


class ValidateKingMovementTest(unittest.TestCase):
    """ Test class for Queen Movement """

    def test_queen_can_move_diagonal(self):
        test_piece = "Qf3"

        actual = get_board_moves(test_piece, "", test_piece)
        expected = {
            Coordinates("e4"),
            Coordinates("g4"),
            Coordinates("e2"),
            Coordinates("g2")
        }

        for coordinate in expected:
            self.assertIn(coordinate, actual)

    def test_queen_can_move_straight(self):
        test_piece = "Qf3"

        actual = get_board_moves(test_piece, "", test_piece)
        expected = {
            Coordinates("f4"),
            Coordinates("f2"),
            Coordinates("e3"),
            Coordinates("g3")
        }

        for coordinate in expected:
            self.assertIn(coordinate, actual)
        self.assertTrue(expected.issubset(actual))

    def test_queen_can_move_straight_three(self):
        test_piece = "Qd4"

        actual = get_board_moves(test_piece, "", test_piece)
        expected = {
            Coordinates("d1"),
            Coordinates("d7"),
            Coordinates("a4"),
            Coordinates("g4")
        }

        for coordinate in expected:
            self.assertIn(coordinate, actual)

    def test_all_queen_movements_unrestricted(self):
        test_piece = "Qd4"

        actual = get_board_moves(test_piece, "", test_piece)
        expected = {
            Coordinates("c3"),
            Coordinates("b2"),
            Coordinates("a1"),
            Coordinates("e3"),
            Coordinates("f2"),
            Coordinates("g1"),
            Coordinates("c5"),
            Coordinates("b6"),
            Coordinates("a7"),
            Coordinates("e5"),
            Coordinates("f6"),
            Coordinates("g7"),
            Coordinates("h8"),
            Coordinates("c4"),
            Coordinates("b4"),
            Coordinates("a4"),
            Coordinates("e4"),
            Coordinates("f4"),
            Coordinates("g4"),
            Coordinates("h4"),
            Coordinates("d3"),
            Coordinates("d2"),
            Coordinates("d1"),
            Coordinates("d5"),
            Coordinates("d6"),
            Coordinates("d7"),
            Coordinates("d8")
        }

        self.assertEqual(expected, actual)
