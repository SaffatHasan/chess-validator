""" Test module for adding pieces to the board """
# pylint: disable=C0111

import unittest
from src.main.python.state.position_state import Coordinates
from src.test.python.test_util import get_board_moves


class ValidatePawnMovementTest(unittest.TestCase):
    """ Test class for Pawn movement """

    def test_white_pawn_moves_forward_one(self):
        test_piece = "Pa1"
        actual = get_board_moves(test_piece, "", test_piece)
        expected = Coordinates("a2")

        self.assertIn(expected, actual)

    def test_black_pawn_moves_forward_one(self):
        test_piece = "Pa8"
        actual = get_board_moves("", test_piece, test_piece)
        expected = Coordinates("a7")

        self.assertIn(expected, actual)

    def test_white_pawn_move_forward_two(self):
        test_piece = "Pa2"
        actual = get_board_moves(test_piece, "", test_piece)
        expected = Coordinates("a4")

        self.assertIn(expected, actual)

    def test_black_pawn_move_forward_two(self):
        test_piece = "Pa7"
        actual = get_board_moves("", test_piece, test_piece)
        expected = Coordinates("a5")

        self.assertIn(expected, actual)

    def test_black_pawn_eat_forward_left(self):
        test_piece = "Pb2"
        actual = get_board_moves("Pc1", test_piece, test_piece)
        expected = Coordinates("c1")

        self.assertIn(expected, actual)

    def test_black_pawn_eat_forward_right(self):
        test_piece = "Pb2"
        actual = get_board_moves("Pa1", test_piece, test_piece)
        expected = Coordinates("a1")

        self.assertIn(expected, actual)

    def test_pawn_does_not_move_diagonal_if_no_enemy_piece(self):
        test_piece = "Pb2"
        actual = get_board_moves("", test_piece, test_piece)
        not_expected = Coordinates("a1")

        self.assertNotIn(not_expected, actual)

    def test_pawn_does_not_move_through_enemy_pieces(self):
        test_piece = "Pb2"
        actual = get_board_moves("Pb1", test_piece, test_piece)
        not_expected = Coordinates("b1")
        self.assertNotIn(not_expected, actual)

    def test_pawn_does_not_move_through_friendly_pieces(self):
        test_piece = "Pb2"
        black_pieces = test_piece + ", Pb1"
        actual = get_board_moves("", black_pieces, test_piece)
        not_expected = Coordinates("b1")
        self.assertNotIn(not_expected, actual)

    def test_pawn_does_not_move_twice_on_top_of_friendly_pieces(self):
        test_piece = "Pb7"
        black_pieces = test_piece + ", Pb5"
        actual = get_board_moves("", black_pieces, test_piece)
        not_expected = Coordinates("b5")
        self.assertNotIn(not_expected, actual)

    def test_pawn_does_not_move_twice_through_piece(self):
        test_piece = "Pb7"
        black_pieces = test_piece + ", Pb6"
        actual = get_board_moves("", black_pieces, test_piece)
        not_expected = Coordinates("b5")
        self.assertNotIn(not_expected, actual)
