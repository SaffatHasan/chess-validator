""" Test module for adding pieces to the board """
# pylint: disable=C0111,C0103

import unittest
from src.main.python.state.piece_enums import PieceTypes, Color


class EnumTest(unittest.TestCase):
    """ Test class for Colors and PieceType representations """

    def test_white_to_string_is_W(self):
        actual = str(Color.WHITE)
        expected = 'W'

        self.assertEqual(expected, actual)

    def test_pawn_to_string_is_P(self):
        actual = str(PieceTypes.PAWN)
        expected = "P"

        self.assertEqual(expected, actual)
