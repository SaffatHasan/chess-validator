""" Test module for adding pieces to the board """
# pylint: disable=C0111

import unittest
from src.main.python.motion import board_movement
from src.main.python.state.position_state import Coordinates
from src.main.python.state.piece_enums import Color
from src.main.python.util import compose


class ValidateMovementTest(unittest.TestCase):
    """ Test class for UserInput """

    def test_forward_off_board_returns_none(self):
        start = Coordinates("a8")
        actual = board_movement.move_north(start)

        self.assertIsNone(actual)

    def test_composition_of_none_propagates_through_movement_engine(self):
        start = None

        sample_move_function = compose(
            board_movement.move_north,
            board_movement.move_south,
            board_movement.move_east,
            board_movement.move_west
        )
        actual = sample_move_function(start)

        self.assertIsNone(actual)

    def test_a1_white_forward_is_a2(self):
        forward = board_movement.get_forward_by_color(Color.WHITE)
        coordinates = Coordinates("a1")

        actual = forward(coordinates)
        expected = Coordinates("a2")

        self.assertEqual(expected, actual)

    def test_a2_black_forward_is_a1(self):
        forward = board_movement.get_forward_by_color(Color.BLACK)
        coordinates = Coordinates("a2")

        actual = forward(coordinates)
        expected = Coordinates("a1")

        self.assertEqual(expected, actual)

    def test_c4_white_left_is_b4(self):
        left = board_movement.get_left_by_color(Color.WHITE)
        coordinates = Coordinates("c4")

        actual = left(coordinates)
        expected = Coordinates("b4")

        self.assertEqual(expected, actual)

    def test_c4_white_right_is_d4(self):
        right = board_movement.get_right_by_color(Color.WHITE)
        coordinates = Coordinates("c4")

        actual = right(coordinates)
        expected = Coordinates("d4")

        self.assertEqual(expected, actual)

    def test_c4_black_left_is_d4(self):
        left = board_movement.get_left_by_color(Color.BLACK)
        coordinates = Coordinates("c4")

        actual = left(coordinates)
        expected = Coordinates("d4")

        self.assertEqual(expected, actual)

    def test_c4_black_right_is_b4(self):
        right = board_movement.get_right_by_color(Color.BLACK)
        coordinates = Coordinates("c4")

        actual = right(coordinates)
        expected = Coordinates("b4")

        self.assertEqual(expected, actual)

    def test_b2_black_forward_left_is_c1(self):
        forward = board_movement.get_forward_by_color(Color.BLACK)
        left = board_movement.get_left_by_color(Color.BLACK)
        forward_left = compose(forward, left)

        coordinates = Coordinates("b2")

        actual = forward_left(coordinates)
        expected = Coordinates("c1")

        self.assertEqual(expected, actual)
