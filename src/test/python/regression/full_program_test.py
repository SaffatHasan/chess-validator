""" Test module to get moves which do not put a friendly
    king into check """
# pylint: disable=C0111, W0201

import unittest
from unittest.mock import MagicMock

import pytest

from src.main.python.driver import show_moves
from src.main.python.system_wrappers.wrappers import PrintWrapper
from src.main.python.user_io.board_output import ConsoleOutput
from src.test.python.test_util import setup_board


class FullRegressionTest(unittest.TestCase):
    """ Test class for End to End what do we actually output? """

    @pytest.fixture(autouse=True)
    def run_around_tests(self):
        # Code that will run before your test
        self.print_wrapper = PrintWrapper()
        self.print_wrapper.print = MagicMock()
        self.console_output = ConsoleOutput(self.print_wrapper)
        # A test function will be run at this point
        yield
        # Code that will run after your test
        board = setup_board(self.white_pieces, self.black_pieces)
        show_moves(board, self.piece_to_move, self.console_output)
        expected = "LEGAL MOVES FOR {}: {}".format(
            self.piece_to_move, self.moves)
        self.print_wrapper.print.assert_called_with(expected)

    def test_given_example_from_assignment(self):
        """  the Rook located on square f1 can legally move only to any
        of the following squares: e1, d1, c1, b1, a1 """
        self.white_pieces = "Rf1, Pf2, Bf5, Kg1, Pg3, Ph2"
        self.black_pieces = "Ra5, Pa7, Pb7, Kb8, Pc7, Be8"
        self.piece_to_move = "Rf1"
        self.moves = "a1, b1, c1, d1, e1"

    def test_bishop_in_assignment_example(self):
        self.white_pieces = "Rf1, Pf2, Bf5, Kg1, Pg3, Ph2"
        self.black_pieces = "Ra5, Pa7, Pb7, Kb8, Pc7, Be8"
        self.piece_to_move = "Be8"
        self.moves = "a4, b5, c6, d7, f7, g6, h5"

    def test_pawn_when_blocking_possible_check(self):
        """ Moving this pawn would put black king in check """
        self.white_pieces = "Rf1, Pf2, Bf4, Kg1, Pg3, Ph2"
        self.black_pieces = "Ra5, Pa7, Pb7, Kb8, Pc7, Be8"
        self.piece_to_move = "Pc7"
        self.moves = ""
