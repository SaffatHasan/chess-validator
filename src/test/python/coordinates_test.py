""" Test module for Coordinates """
# pylint: disable=C0111
import unittest
from src.main.python.state.position_state import Coordinates


class CoordinatesTest(unittest.TestCase):
    """ Test class for UserInput """

    def test_coordinates_contains_x_and_y(self):
        """ Ensure x and y are returned from coordinates """
        coordinate = Coordinates("a1")

        actual = coordinate.get_x()
        expected = 0

        self.assertEqual(actual, expected)

        actual = coordinate.get_y()
        expected = 0

        self.assertEqual(actual, expected)

    def test_b2_has_position_1_1(self):
        """ Validate B2 = (1,1) """
        coordinate = Coordinates("b2")

        actual = coordinate.get_x()
        expected = 1

        self.assertEqual(actual, expected)

        actual = coordinate.get_y()
        expected = 1

        self.assertEqual(actual, expected)

    def test_h8_has_position_7_7(self):
        """ Validate H8 = (7,7) """
        coordinate = Coordinates("h8")

        actual = coordinate.get_x()
        expected = 7

        self.assertEqual(actual, expected)

        actual = coordinate.get_y()
        expected = 7

        self.assertEqual(actual, expected)

    def test_parse_0_0_is_a1(self):
        expected = Coordinates("a1")
        actual = Coordinates.parse_int_pair(0, 0)

        self.assertEqual(expected, actual)

    def test_0_8_is_not_valid(self):
        actual = Coordinates.is_int_pair_valid(0, 8)
        self.assertFalse(actual)

    def test_8_0_is_not_valid(self):
        actual = Coordinates.is_int_pair_valid(8, 0)
        self.assertFalse(actual)

    def test_0_neg_1_is_not_valid(self):
        actual = Coordinates.is_int_pair_valid(0, -1)
        self.assertFalse(actual)

    def test_neg_1_0_is_not_valid(self):
        actual = Coordinates.is_int_pair_valid(-1, 0)
        self.assertFalse(actual)

    def test_a1_is_represented_as_a1(self):
        actual = repr(Coordinates("a1"))
        expected = "a1"

        self.assertEqual(expected, actual)

    def test_parse_9_9_returns_none(self):
        actual = Coordinates.parse_int_pair(9, 9)
        self.assertIsNone(actual)
