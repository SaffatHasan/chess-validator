""" File: test_util.py

    contains common code between tests
"""
from src.main.python.user_io.board_input import ConsoleInput
from src.main.python.state.board_state import Board
from src.main.python.services.input_engine import InputToBoardEngine
from src.main.python.motion.piece_movement import MoveEngine


def setup_board(white_pieces_str, black_pieces_str):
    """ Returns a board as specified by the io parameters

        e.g. setup_board("Rf1", "Rf2") would set the board with

            White Rook f1
            Black Rook f2
    """
    board = Board()
    mocked_input = get_mocked_input(white_pieces_str, black_pieces_str)
    input_engine = InputToBoardEngine(mocked_input, board)
    input_engine.add_pieces_from_input()
    return board


def get_mocked_input(white_pieces_str, black_pieces_str):
    """ Returns a user_input class with mocked return values """
    mocked_input = ConsoleInput(None)
    mocked_input.get_white_pieces_input = lambda: white_pieces_str
    mocked_input.get_black_pieces_input = lambda: black_pieces_str
    return mocked_input


def get_board_moves(white_pieces_str, black_pieces_str, piece_to_move):
    """ Returns a user_input class with mocked return values """
    board = setup_board(white_pieces_str, black_pieces_str)
    move_engine = MoveEngine(board, piece_to_move)
    return move_engine.get_moves()
