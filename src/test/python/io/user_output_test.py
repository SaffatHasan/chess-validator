""" Test module to get moves which do not put a friendly
    king into check """
# pylint: disable=C0111

import unittest
from unittest.mock import MagicMock

from src.main.python.state.position_state import Coordinates
from src.main.python.system_wrappers.wrappers import PrintWrapper
from src.main.python.user_io.board_output import ConsoleOutput
from src.main.python.exceptions.board_exceptions import InvalidBoardStateException


class OutputServiceTest(unittest.TestCase):
    """ Tests print """

    @staticmethod
    def test_print_coordinates():
        """ Test that printing coordinates outputs what is expected """
        print_wrapper = PrintWrapper()
        print_wrapper.print = MagicMock()

        moves = {Coordinates("a1"), Coordinates("a2")}
        piece_str = "Rf1"
        console_output = ConsoleOutput(print_wrapper)
        console_output.print_moves(piece_str, moves)

        print_wrapper.print.assert_called_with("LEGAL MOVES FOR Rf1: a1, a2")

    @staticmethod
    def test_printing_error():
        print_wrapper = PrintWrapper()
        print_wrapper.print = MagicMock()
        error = InvalidBoardStateException("foo")
        console_output = ConsoleOutput(print_wrapper)
        console_output.print_error(error)

        print_wrapper.print.assert_called_with("Encountered error: foo")
