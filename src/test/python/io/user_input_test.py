""" Test module for UserInput """

import unittest
from unittest.mock import MagicMock
from src.main.python.user_io.board_input import ConsoleInput
from src.main.python.system_wrappers.wrappers import InputWrapper


class UserInputTest(unittest.TestCase):
    """ Test class for UserInput """

    def setUp(self):
        self.console_input = InputWrapper()
        self.console_input.input = MagicMock(return_value="foo")
        self.user_input = ConsoleInput(self.console_input)

    def test_white_pieces_prompt(self):
        """ UserInput should say
                WHITE:
        """
        self.user_input.get_white_pieces_input()
        self.console_input.input.assert_called_with("WHITE: ")

    def test_black_pieces_prompt(self):
        """ UserInput should say
                BLACK:
        """
        self.user_input.get_black_pieces_input()
        self.console_input.input.assert_called_with("BLACK: ")

    def test_piece_to_move_prompt(self):
        """ UserInput should say
                PIECE TO MOVE:
        """
        self.user_input.get_piece_to_move()
        self.console_input.input.assert_called_with("PIECE TO MOVE: ")

    def test_user_input_returns_string(self):
        """ Enforce that user io only returns strings """
        actual = self.user_input.get_white_pieces_input()
        self.assertTrue(isinstance(actual, str))

        actual = self.user_input.get_black_pieces_input()
        self.assertTrue(isinstance(actual, str))

        actual = self.user_input.get_piece_to_move()
        self.assertTrue(isinstance(actual, str))
