""" Test module for adding pieces to the board """
# pylint: disable=C0111

import unittest

from src.main.python.exceptions.board_exceptions import InvalidAddException
from src.main.python.system_wrappers.wrappers import InputWrapper
from src.main.python.user_io.board_input import ConsoleInput
from src.main.python.state.position_state import Coordinates
from src.main.python.state.board_state import Board
from src.main.python.state.piece_state import Piece
from src.main.python.state.piece_enums import PieceTypes, Color
from src.main.python.services.input_engine import InputToBoardEngine


class AddPiecesToBoardTest(unittest.TestCase):
    def setUp(self):
        self.mocked_input = ConsoleInput(None)
        self.board = Board()
        self.input_engine = InputToBoardEngine(self.mocked_input, self.board)

    def test_add_white_rook_to_board(self):
        self.mocked_input.get_white_pieces_input = lambda: "Rf1"
        self.input_engine.add_white_pieces()

        coordinate = Coordinates("f1")
        actual = self.board.get_piece_at_coordinate(coordinate)
        expected = Piece(PieceTypes.ROOK, Color.WHITE)
        self.assertEqual(expected, actual)

    def test_add_black_rook_to_board(self):
        self.mocked_input.get_black_pieces_input = lambda: "Rf1"
        self.input_engine.add_black_pieces()

        coordinate = Coordinates("f1")
        actual = self.board.get_piece_at_coordinate(coordinate)
        expected = Piece(PieceTypes.ROOK, Color.BLACK)
        self.assertEqual(expected, actual)

    def test_add_white_and_black_to_board(self):
        self.mocked_input.get_white_pieces_input = lambda: "Rf1"
        self.mocked_input.get_black_pieces_input = lambda: "Rf2"

        self.input_engine.add_pieces_from_input()

        coordinate = Coordinates("f1")
        actual = self.board.get_piece_at_coordinate(coordinate)
        expected = Piece(PieceTypes.ROOK, Color.WHITE)
        self.assertEqual(expected, actual)

        coordinate = Coordinates("f2")
        actual = self.board.get_piece_at_coordinate(coordinate)
        expected = Piece(PieceTypes.ROOK, Color.BLACK)
        self.assertEqual(expected, actual)

    def test_add_two_pieces_to_same_spot_raises_exception(self):
        self.mocked_input.get_white_pieces_input = lambda: "Rf1"
        self.mocked_input.get_black_pieces_input = lambda: "Rf1"

        self.assertRaises(
            InvalidAddException,
            self.input_engine.add_pieces_from_input)

    def test_add_piece_with_bad_type_raises_invalid_add_exception(self):
        self.mocked_input.get_white_pieces_input = lambda: "Zf1"
        self.mocked_input.get_black_pieces_input = lambda: "Rf1"

        self.assertRaises(
            InvalidAddException,
            self.input_engine.add_pieces_from_input)

    def test_add_piece_with_bad_coordinates_raises_invalid_add_exception(self):
        self.mocked_input.get_white_pieces_input = lambda: "K99"
        self.mocked_input.get_black_pieces_input = lambda: "Rf1"

        self.assertRaises(
            InvalidAddException,
            self.input_engine.add_pieces_from_input)

    def test_add_piece_with_partial_bad_coordinates_raises_invalid_add_exception(
            self):
        self.mocked_input.get_white_pieces_input = lambda: "Kf9"
        self.mocked_input.get_black_pieces_input = lambda: "Rf1"

        self.assertRaises(
            InvalidAddException,
            self.input_engine.add_pieces_from_input)

    @staticmethod
    def test_integration_with_user_input_engine():
        """ Check that InputToBoardEngine works with ConsoleInput """
        input_wrapper = InputWrapper()
        input_wrapper.input = lambda _: ""
        console_input = ConsoleInput(input_wrapper)

        input_engine = InputToBoardEngine(console_input, Board())
        input_engine.add_pieces_from_input()
