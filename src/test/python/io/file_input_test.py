""" Test module for UserInput """
# pylint: disable=C0111
import unittest
import os
from src.main.python.user_io.board_input import FileInput


class FileInputTest(unittest.TestCase):
    """ Test class for UserInput """

    def test_file_not_found_raises_error(self):
        """ FileInput should raise an error
        """
        file_name = "File_that_does_not_exist"
        self.assertRaises(FileNotFoundError, FileInput, file_name)

    def test_get_white_pieces_from_file_1(self):
        """ Test path is project/src/test/python
            Input files  project/src/test/resources/io/*.txt
        """
        file_name = "sample_game_1.txt"
        file_path = get_test_file_path(file_name)
        user_input = FileInput(file_path)
        actual = user_input.get_white_pieces_input()
        expected = "Rf1, Kg1, Pf2, Ph2, Pg3"
        self.assertEqual(expected, actual)

    def test_get_white_pieces_from_file_2(self):
        """ Test path is project/src/test/python
            Input files  project/src/test/resources/io/*.txt
        """
        file_name = "sample_game_2.txt"
        file_path = get_test_file_path(file_name)
        user_input = FileInput(file_path)
        actual = user_input.get_white_pieces_input()
        expected = "Rf1, Kg1, Pf2, Ph2"
        self.assertEqual(expected, actual)

    def test_get_black_piece_from_file_1(self):
        file_name = "sample_game_1.txt"
        file_path = get_test_file_path(file_name)
        user_input = FileInput(file_path)
        actual = user_input.get_black_pieces_input()
        expected = "Kb8, Ne8, Pa7, Pb7, Pc7, Ra5"
        self.assertEqual(expected, actual)

    def test_get_black_piece_from_file_2(self):
        file_name = "sample_game_2.txt"
        file_path = get_test_file_path(file_name)
        user_input = FileInput(file_path)
        actual = user_input.get_black_pieces_input()
        expected = "Kb8, Ne8, Pa7, Pb7, Pc7"
        self.assertEqual(expected, actual)

    def test_get_expected_from_file_1(self):
        file_name = "sample_game_1.txt"
        file_path = get_test_file_path(file_name)
        user_input = FileInput(file_path)
        actual = user_input.get_piece_to_move()
        expected = "Rf1"
        self.assertEqual(expected, actual)

    def test_get_expected_from_file_2(self):
        file_name = "sample_game_2.txt"
        file_path = get_test_file_path(file_name)
        user_input = FileInput(file_path)
        actual = user_input.get_piece_to_move()
        expected = "Kb8"
        self.assertEqual(expected, actual)


def get_test_input_path():
    """ Gets the path to src/test/resources/io """
    return os.path.join(
        os.path.dirname(__file__),
        '..',
        '..',
        'resources',
        'input')


def get_test_file_path(file_name):
    """ Gets the path to src/test/resources/io/$file_name """
    test_dir_path = get_test_input_path()
    return os.path.join(test_dir_path, file_name)
