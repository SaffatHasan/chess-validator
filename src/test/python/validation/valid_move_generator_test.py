""" Test module to get moves which do not put a friendly
    king into check """
# pylint: disable=C0111

import unittest

from src.main.python.services.valid_moves import get_valid_moves
from src.main.python.state.position_state import Coordinates
from src.test.python.test_util import setup_board


class ValidateMoveGeneratorTest(unittest.TestCase):
    """ Test class for King Movement """

    def test_moving_rook_puts_king_in_check(self):
        """ Should see a total of 8 moves """
        board = setup_board("Ka1, Ra2", "Qa3")

        actual = get_valid_moves(board, "Ra2")
        expected = {Coordinates("a3")}

        self.assertEqual(expected, actual)
