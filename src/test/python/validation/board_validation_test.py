""" Test module for adding pieces to the board """
# pylint: disable=C0111

import unittest
from src.main.python.services.board_state_validation_engine import ValidationEngine
from src.main.python.exceptions.board_exceptions import InvalidBoardStateException
from src.main.python.state.piece_enums import Color
from src.main.python.state.piece_state import King
from src.test.python.test_util import setup_board


class ValidateBoardTest(unittest.TestCase):
    def test_no_kings_on_board_raises_invalid_board_state_error(self):
        board = setup_board("Rf1", "Rf2")

        validation_engine = ValidationEngine(board)
        self.assertRaises(
            InvalidBoardStateException,
            validation_engine.validate)

    def test_only_white_king_on_board_raises_invalid_board_state_error(self):
        board = setup_board("Rf1, Kf3", "Rf2")

        validation_engine = ValidationEngine(board)
        self.assertRaises(
            InvalidBoardStateException,
            validation_engine.validate)

    def test_count_kings_on_board(self):
        board = setup_board(
            "Rf1, Pf2, Bf5, Kg1, Pg3, Ph2",
            "Ra5, Pa7, Pb7, Kb8, Pc7, Be8")
        validation_engine = ValidationEngine(board)
        expected = 1
        target_piece = King(Color.WHITE)
        actual = validation_engine.get_piece_count(target_piece)

        self.assertEqual(expected, actual)

    def test_count_num_white_kings(self):
        board = setup_board(
            "Rf1, Pf2, Bf5, Kg1, Pg3, Ph2",
            "Ra5, Pa7, Pb7, Kb8, Pc7, Be8")
        validation_engine = ValidationEngine(board)
        expected = 1
        actual = validation_engine.get_num_white_kings()

        self.assertEqual(expected, actual)

    def test_too_many_white_kings_raises_exception(self):
        board = setup_board(
            "Rf1, Pf2, Bf5, Kg1, Pg3, Ph2, Ka1",
            "Ra5, Pa7, Pb7, Kb8, Pc7, Be8")
        validation_engine = ValidationEngine(board)

        self.assertRaises(
            InvalidBoardStateException,
            validation_engine.validate)

    def test_too_many_black_kings_raises_exception(self):
        board = setup_board(
            "Rf1, Pf2, Bf5, Kg1, Pg3, Ph2",
            "Ra5, Pa7, Pb7, Kb8, Pc7, Be8, Ka1")
        validation_engine = ValidationEngine(board)
        self.assertRaises(
            InvalidBoardStateException,
            validation_engine.validate)

    @staticmethod
    def test_valid_board_raises_no_exceptions():
        board = setup_board(
            "Rf1, Kg1, Pf2, Ph2, Pg3",
            "Kb8, Ne8, Pa7, Pb7, Pc7, Ra5")

        validation_engine = ValidationEngine(board)
        validation_engine.validate()
