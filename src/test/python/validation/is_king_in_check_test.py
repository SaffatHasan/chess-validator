""" Test module for adding pieces to the board """
# pylint: disable=C0111,C0301

import unittest

from src.main.python.state.position_state import Coordinates
from src.test.python.test_util import setup_board
from src.main.python.services.king_check import is_king_in_check, get_king_coordinates, get_enemy_pieces, \
    get_all_enemy_moves
from src.main.python.state.piece_enums import Color


class KingInCheckTest(unittest.TestCase):
    """ Test class for UserInput """

    def test_get_king_coordinates_returns_f1_when_white_king_is_f1(self):
        board = setup_board("Kf1", "Pf2")
        actual = get_king_coordinates(board, Color.WHITE)
        expected = Coordinates("f1")
        self.assertEqual(expected, actual)

    def test_king_alone_is_not_in_check(self):
        board = setup_board("Kf1", "")
        actual = is_king_in_check(board, Color.WHITE)
        self.assertFalse(actual)

    def test_when_getting_black_king_coordinates_white_king_coordinates_not_returned(
            self):
        board = setup_board("Kf1", "Kf2")
        actual = get_king_coordinates(board, Color.BLACK)
        expected = Coordinates("f2")
        self.assertEqual(expected, actual)

    def test_none_is_returned_when_asking_for_black_king_and_only_white_king_exists(
            self):
        board = setup_board("Kf1", "")
        actual = get_king_coordinates(board, Color.BLACK)
        self.assertIsNone(actual)

    def test_get_enemy_pieces_returns_enemies_as_piece_strings(self):
        enemy_string = "Nf5, Bf6"
        board = setup_board("Kf1, Pf2", enemy_string)
        actual = get_enemy_pieces(board, Color.WHITE)
        expected = enemy_string.split(", ")

        self.assertListEqual(expected, actual)

    def test_get_all_enemy_moves_returns_two_moves_with_two_pawns(self):
        board = setup_board("", "Pf4, Pg4")
        actual = get_all_enemy_moves(board, Color.WHITE)
        expected = {Coordinates("f3"), Coordinates("g3")}
        self.assertEqual(expected, actual)

    def test_king_with_friendlies_is_not_in_check(self):
        board = setup_board("Kf1, Qf2, Pf3", "")
        actual = is_king_in_check(board, Color.WHITE)
        self.assertFalse(actual)

    def test_king_in_check_by_pawn_returns_true(self):
        board = setup_board("Kf1", "Pe2")
        actual = is_king_in_check(board, Color.WHITE)
        self.assertTrue(actual)
