""" File: board_exceptions.py

Contains exceptions relating to the chess board state
"""


class InvalidPieceException(Exception):
    """ Raised when an invalid piece is specified """


class InvalidAddException(Exception):
    """ Raised when a piece is added to a non-empty position """


class InvalidBoardStateException(Exception):
    """ Raised when the board is in an invalid state """
