""" Validates a board """
from src.main.python.exceptions.board_exceptions import InvalidBoardStateException
from src.main.python.state.piece_enums import PieceTypes, Color
from src.main.python.state.piece_state import Piece


class ValidationEngine:
    """ Validates a board to see if it is in a correct state
    i.e. that the number of pieces adds up to a valid number """

    def __init__(self, board):
        """ Setup validation engine """
        self.board = board
        self.board_pieces = self.board.get_board_as_dict()

    def validate(self):
        """ Validation method calls all validation functions """
        self.validate_num_kings()

    def validate_num_kings(self):
        """ Validate that there is exactly 1 king of each type on the board """
        white_kings = self.get_num_white_kings()
        black_kings = self.get_num_black_kings()

        if white_kings == 0:
            raise InvalidBoardStateException("No white kings on board")

        if white_kings > 1:
            raise InvalidBoardStateException("More than 1 white king on board")

        if black_kings == 0:
            raise InvalidBoardStateException("No black kings on board")

        if black_kings > 1:
            raise InvalidBoardStateException("More than 1 black king on board")

    def get_num_white_kings(self):
        """ Gets the number of white kings """
        white_king = Piece(PieceTypes.KING, Color.WHITE)
        return self.get_piece_count(white_king)

    def get_num_black_kings(self):
        """ Gets the number of black kings """
        black_king = Piece(PieceTypes.KING, Color.BLACK)
        return self.get_piece_count(black_king)

    def get_piece_count(self, target_piece):
        """ Gets the count associated with a particular piece (type, color pair) """
        count = 0
        for position in self.board_pieces:
            piece_on_board = self.board.get_piece_at_coordinate(position)
            if piece_on_board == target_piece:
                count += 1
        return count
