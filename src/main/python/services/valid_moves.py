""" Service that will return list of valid moves, checking for checkmates """
from src.main.python.motion.board_movement import move_piece_on_board
from src.main.python.motion.piece_movement import MoveEngine
from src.main.python.services.input_engine import InputToBoardEngine
from src.main.python.services.king_check import is_king_in_check


def get_valid_moves(board, piece_string):
    """ Takes the additional step of making sure the pieces
        don't cause your King to be in check """

    engine = MoveEngine(board, piece_string)
    moves = engine.get_moves()
    original_coordinates = InputToBoardEngine.get_piece_coordinates_from_string(
        piece_string)
    valid_moves = set()
    for move in moves:
        if does_move_cause_king_to_go_in_check(
                move, board, original_coordinates):
            continue
        valid_moves.add(move)

    return valid_moves


def does_move_cause_king_to_go_in_check(
        move, board, coordinates_of_piece_to_move):
    """ Returns if moving a piece from coordinate_a to coordinate_b causes king to go into check """
    color = board.get_piece_at_coordinate(
        coordinates_of_piece_to_move).get_color()
    resulting_board = move_piece_on_board(
        board, coordinates_of_piece_to_move, move)
    return is_king_in_check(resulting_board, color)
