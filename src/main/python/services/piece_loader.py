""" file piece_loader.py

Meant to contain all the logic, might need some refactoring
"""

from src.main.python.state.piece_enums import PieceTypes
from src.main.python.state.piece_state import King, Queen, Bishop, Knight, Rook, Pawn


# pylint: disable=R0903,R0201


class PieceLoader:
    """ Class for piece loader """
    piece_map = {
        PieceTypes.KING: King,
        PieceTypes.QUEEN: Queen,
        PieceTypes.BISHOP: Bishop,
        PieceTypes.KNIGHT: Knight,
        PieceTypes.ROOK: Rook,
        PieceTypes.PAWN: Pawn
    }

    @classmethod
    def get_piece_class(cls, piece_type):
        """ Returns the piece class associated with a type """
        return cls.piece_map[piece_type]
