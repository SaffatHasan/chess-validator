""" Modules that determines if a king is in check for a given board state """

from src.main.python.motion.piece_movement import MoveEngine
from src.main.python.state.piece_enums import PieceTypes


def is_king_in_check(board, color):
    """ Returns true if king is in check """
    king_coordinates = get_king_coordinates(board, color)
    enemy_moves = get_all_enemy_moves(board, color)
    return king_coordinates in enemy_moves


def get_king_coordinates(board, color):
    """ Returns the position of the king """
    board_dict = board.get_board_as_dict()
    for position in board_dict:
        piece = board_dict[position]
        if piece.get_type() != PieceTypes.KING:
            continue
        if piece.get_color() != color:
            continue
        return position
    return None


def get_all_enemy_moves(board, color):
    """ Returns a ist of all enemy moves """
    enemy_pieces = get_enemy_pieces(board, color)

    enemy_moves = set()

    for piece in enemy_pieces:
        engine = MoveEngine(board, piece)
        enemy_moves = enemy_moves | engine.get_moves()

    return enemy_moves


def get_enemy_pieces(board, color):
    """ Returns a list of enemy piece strings """
    board_dict = board.get_board_as_dict()
    enemy_pieces = []
    for position in board_dict:
        piece = board_dict[position]
        if piece.get_color() != color:
            enemy_pieces += [(position, piece)]

    return [str(piece.get_type()) + str(position)
            for (position, piece) in enemy_pieces]
