""" Sets up board from specified input """
from src.main.python.exceptions.board_exceptions import InvalidAddException
from src.main.python.services.piece_loader import PieceLoader
from src.main.python.state.piece_enums import Color, PieceTypes
from src.main.python.state.position_state import Coordinates


class InputToBoardEngine:
    """ Engine that takes user io and board classes and
        manages data that goes between

        Specifically used to set up the board and perform
        validation.
    """

    def __init__(self, user_input, board):
        """ Initializes class with specified user_input (Console or File) and a board """
        self.user_input = user_input
        self.board = board

    def add_pieces_from_input(self):
        """ Gets user io and adds to board for both white and black io """
        self.add_white_pieces()
        self.add_black_pieces()

    def add_white_pieces(self):
        """" Adds white pieces to the board """
        pieces_string = self.get_white_pieces()
        self.add_pieces_from_list_of_strings(pieces_string, Color.WHITE)

    def add_black_pieces(self):
        """" Adds black pieces to the board """
        pieces_string = self.get_black_pieces()
        self.add_pieces_from_list_of_strings(pieces_string, Color.BLACK)

    def get_white_pieces(self):
        """ Gets the white pieces from user io and
        sets up the board """
        _input = self.user_input.get_white_pieces_input()
        return _input.split()

    def get_black_pieces(self):
        """ Gets the white pieces from user io and
        sets up the board """
        _input = self.user_input.get_black_pieces_input()
        return _input.split()

    def add_pieces_from_list_of_strings(self, pieces_string, color):
        """ Adds the pieces from list of strings to the board with specified color """
        for item in pieces_string:
            self.add_piece_from_string(item, color)

    def add_piece_from_string(self, piece_string, color):
        """ Adds the piece from a string to the board with specified color """
        piece = self.get_piece_from_string(piece_string, color)
        coordinates = self.get_piece_coordinates_from_string(piece_string)
        if coordinates in self.board.board_objects:
            raise InvalidAddException(
                "Coordinates {} is already occupied by another piece".format(
                    str(coordinates)))
        self.board.add_piece_to_board(piece, coordinates)

    def get_piece_from_string(self, piece_string, color):
        """ Returns a piece from given string and color """
        piece_type = self.get_piece_type_from_string(piece_string)
        piece_class = PieceLoader.get_piece_class(piece_type)
        return piece_class(color)

    @staticmethod
    def get_piece_type_from_string(piece_string):
        """ Returns the piece type from a piece string
            (e.g. "Rf1" -> "R")
        """
        piece_type_string = piece_string[0:1]
        try:
            piece_type = PieceTypes(piece_type_string)
        except ValueError:
            raise InvalidAddException(
                "Invalid Piece Type specified: {}".format(piece_type_string))
        return piece_type

    @staticmethod
    def get_piece_coordinates_from_string(piece_string):
        """ Returns the piece type from a piece string
                    (e.g. "Rf1" -> "f1")
                """
        coordinates_string = piece_string[1:3]
        try:
            Coordinates(coordinates_string)
        except ValueError:
            raise InvalidAddException(
                "Invalid coordinates specified: {}".format(coordinates_string))
        return Coordinates(piece_string[1:3])
