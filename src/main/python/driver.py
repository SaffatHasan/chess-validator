"""
Usage: python3 driver.py

This script is the driver for the chess validation program.
"""
from src.main.python.exceptions.board_exceptions import InvalidBoardStateException, \
    InvalidAddException
from src.main.python.services.valid_moves import get_valid_moves
from src.main.python.services.board_state_validation_engine import ValidationEngine
from src.main.python.user_io import board_input, board_output
from src.main.python.system_wrappers import wrappers
from src.main.python.services.input_engine import InputToBoardEngine
from src.main.python.state.board_state import Board


def main():
    """ Main method which calls and sets up chess validation """
    my_board = Board()

    console_input = get_console_input()
    console_output = get_console_output()

    try:
        setup_board_from_console_input(my_board, console_input)
        validate_board(my_board)
        piece_to_move = get_piece_to_move(console_input)
    except (InvalidBoardStateException, InvalidAddException) as error:
        console_output.print_error(error)
        return

    show_moves(my_board, piece_to_move, console_output)


def show_moves(board, piece_to_move, console_output):
    """ Shows the moves to the user """
    moves = get_valid_moves(board, piece_to_move)
    console_output.print_moves(piece_to_move, moves)


def setup_board_from_console_input(board, console_user_input):
    """ Prompts the user using specified user_input object """
    input_engine = InputToBoardEngine(console_user_input, board)
    input_engine.add_pieces_from_input()


def get_console_input():
    """ Returns the console io class, setup with a concrete input_wrapper """
    my_input_wrapper = wrappers.InputWrapper()
    my_user_input = board_input.ConsoleInput(my_input_wrapper)
    return my_user_input


def get_console_output():
    """ Returns console print class setup with concrete output_wrapper """
    my_output_wrapper = wrappers.PrintWrapper()
    return board_output.ConsoleOutput(my_output_wrapper)


def get_piece_to_move(console_user_input):
    """ Gets piece to move """
    return console_user_input.get_piece_to_move()


def validate_board(board):
    """ Validates a board """
    ValidationEngine(board).validate()


if __name__ == "__main__":
    main()
