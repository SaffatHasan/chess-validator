""" Simple wrapper for the python builtin io function """
# pylint: disable=R0201,R0903


class InputWrapper:
    """ Class that contains io builtin """

    @staticmethod
    def input(message):
        """ Simple wrapper for builtin io function """
        return input(message)


class PrintWrapper:
    """ Class that contains print builtin """

    @staticmethod
    def print(message):
        """ Simple wrapper """
        print(message)
