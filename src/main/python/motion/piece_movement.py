""" Handles the movement of a piece (as of this comment, just pawns) """
from src.main.python.state.piece_enums import PieceTypes, Color
from src.main.python.services.input_engine import InputToBoardEngine
from src.main.python.motion import board_movement
from src.main.python.util import compose, compose_n_times


class MoveEngine:
    """ Generates the moves for a given board for a given piece """
    MAX_MOVES = 8

    def __init__(self, board, piece_string):
        """ Typical setup. Might need to be refactored. """
        self.board = board
        self.piece_string = piece_string
        self.piece = self.get_piece()

    def get_piece(self):
        """ Returns the piece associated with this object """
        coordinates = self.get_piece_coordinates()
        return self.board.get_piece_at_coordinate(coordinates)

    def get_piece_coordinates(self):
        """ Returns the coordinates for the specified piece """
        return InputToBoardEngine.get_piece_coordinates_from_string(
            self.piece_string)

    def get_moves(self):
        """ Returns moves for a piece"""
        regular_moves = self.get_regular_moves()
        attack_moves = self.get_attack_moves()
        special_moves = self.get_special_moves()

        moves = regular_moves | attack_moves | special_moves
        return self.generate_coordinates_from_moves(moves)

    def get_special_moves(self):
        """ Returns the moves for a pawn piece """
        if self.piece.get_type() != PieceTypes.PAWN:
            return set()

        second_row_white = 1
        second_row_black = 6

        second_row = second_row_white if self.piece.color == Color.WHITE else second_row_black

        current_piece_row = self.get_piece_coordinates().get_y()

        if current_piece_row != second_row:
            return set()

        forward = board_movement.get_forward_by_color(self.piece.get_color())

        if not self.is_multi_regular_move_valid(forward, 2):
            return set()

        forward_two = compose(forward, forward)
        return {forward_two}

    def get_attack_moves(self):
        """ Returns attack moves """
        valid_moves = []
        for move in self.piece.get_attack_moves():
            if self.piece.has_multi_moves:
                valid_moves += self.get_multi_attack_moves(move)
            if self.is_multi_attack_move_valid(move, 1):
                valid_moves += [move]
        return set(valid_moves)

    def get_regular_moves(self):
        """ Returns regular moves """
        valid_moves = []
        for move in self.piece.get_regular_moves():
            if self.piece.has_multi_moves:
                valid_moves += self.get_multi_regular_moves(move)
            if self.is_multi_regular_move_valid(move, 1):
                valid_moves += [move]
        return set(valid_moves)

    def get_multi_attack_moves(self, move):
        """ For a given attack move that can be repeated, return a set of
        the ones which are valid """
        return self.get_multi_moves(move, is_attack=True)

    def get_multi_regular_moves(self, move):
        """ For a given regular move that can be repeated, return a set of the
        ones which are valid """
        return self.get_multi_moves(move, is_attack=False)

    def get_multi_moves(self, move, is_attack):
        """ For a given move and given validation function, return set
        of the ones which are valid """
        if is_attack:
            validation_function = self.is_multi_attack_move_valid
        else:
            validation_function = self.is_multi_regular_move_valid

        valid_moves = []
        for i in range(self.MAX_MOVES):
            if validation_function(move, i):
                valid_moves += [compose_n_times(move, i)]
        return set(valid_moves)

    def generate_coordinates_from_moves(self, moves):
        """ Takes a list of moves (or one move) and returns list of coordinates """
        coordinates = self.get_piece_coordinates()
        coordinates_list = [move(coordinates) for move in moves]
        return set(filter(None, coordinates_list))

    def is_multi_regular_move_valid(self, move, repetitions):
        """ Checks that every move from start to end is unoccupied """
        return self.is_multi_move_valid(move, repetitions, is_attack=False)

    def is_multi_attack_move_valid(self, move, repetitions):
        """ Returns true if a multi-move attack is valid or not"""
        return self.is_multi_move_valid(move, repetitions, is_attack=True)

    def is_multi_move_valid(self, move, repetitions, is_attack):
        """ Returns true if a multi-move is valid or not"""
        for i in range(repetitions - 1):
            current_move = compose_n_times(move, i + 1)
            if self.is_occupied(current_move):
                return False

        last_move = compose_n_times(move, repetitions)

        if is_attack:
            return self.has_enemy_piece(last_move)

        if not self.is_occupied(last_move):
            return True

        return False

    def has_enemy_piece(self, movement):
        """ Returns T/F if a movement has an enemy piece """
        if not self.is_occupied(movement):
            return False

        enemy_piece = self.get_piece_at_movement(movement)
        return enemy_piece.color != self.piece.get_color()

    def is_occupied(self, movement):
        """ Returns T/F is a movement is occupied """
        piece = self.get_piece_at_movement(movement)
        return piece is not None

    def get_piece_at_movement(self, movement):
        """ Returns the piece at a movement """
        enemy_coordinate = self.get_coordinates_after_movement(movement)
        return self.board.get_piece_at_coordinate(enemy_coordinate)

    def get_coordinates_after_movement(self, movement):
        """ Returns the coordinates after a move """
        return movement(self.get_piece_coordinates())
