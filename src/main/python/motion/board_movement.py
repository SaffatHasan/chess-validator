""" Allows conversion from direction and start coordinate to a new coordinate
Converts a direction (forward, left, right, up) to (north, south, east, west) """
from copy import deepcopy

from src.main.python.state.position_state import Coordinates
from src.main.python.state.piece_enums import Color
from src.main.python.util import compose


def get_straight_movements():
    """ Returns north, south, east, west """
    return {
        move_north,
        move_south,
        move_east,
        move_west
    }


def get_diagonal_movements():
    """ Returns diagonals, NW, SW, NE, SE """
    return {
        compose(move_north, move_west),
        compose(move_north, move_east),
        compose(move_south, move_west),
        compose(move_south, move_east)
    }


def get_forward_by_color(color):
    """ Returns the forward direction function """
    if color == Color.WHITE:
        return move_north
    return move_south


def get_left_by_color(color):
    """ Returns the left direction function """
    if color == Color.WHITE:
        return move_west
    return move_east


def get_right_by_color(color):
    """ Returns the right direction function """
    if color == Color.WHITE:
        return move_east
    return move_west


def get_backward_by_color(color):
    """ Returns the backward direction function """
    if color == Color.WHITE:
        return move_south
    return move_north


def move_north(coordinate):
    """ Returns "north" of a coordinate """
    if coordinate is None:
        return None
    new_x = coordinate.get_x()
    new_y = coordinate.get_y() + 1
    return Coordinates.parse_int_pair(new_x, new_y)


def move_south(coordinate):
    """ Returns "south" of a coordinate """
    if coordinate is None:
        return None
    new_x = coordinate.get_x()
    new_y = coordinate.get_y() - 1
    return Coordinates.parse_int_pair(new_x, new_y)


def move_west(coordinate):
    """ Returns "south" of a coordinate """
    if coordinate is None:
        return None
    new_x = coordinate.get_x() - 1
    new_y = coordinate.get_y()
    return Coordinates.parse_int_pair(new_x, new_y)


def move_east(coordinate):
    """ Returns "south" of a coordinate """
    if coordinate is None:
        return None
    new_x = coordinate.get_x() + 1
    new_y = coordinate.get_y()
    return Coordinates.parse_int_pair(new_x, new_y)


def move_piece_on_board(board, coordinate_a, coordinate_b):
    """ Returns a board after moving a piece from coordinate A to coordinate B """
    new_board = deepcopy(board)
    piece = new_board.get_piece_at_coordinate(coordinate_a)

    new_board.remove_piece_at_coordinate(coordinate_a)
    new_board.remove_piece_at_coordinate(coordinate_b)

    new_board.add_piece_to_board(piece, coordinate_b)
    return new_board
