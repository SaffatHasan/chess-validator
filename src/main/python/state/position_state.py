""" Contains the Coordinates class and validation logic """


class Coordinates:
    """ Coordinates are a string, position it contains is a tuple of integers """
    X_COORDINATE_STRING = "abcdefgh"
    Y_COORDINATE_STRING = "12345678"

    def __init__(self, string):
        self.coordinate = string
        self.validate()

    def validate(self):
        """ Validates a coordinate """
        self.validate_x()
        self.validate_y()

    def validate_x(self):
        """ Validates the X value of the coordinate string """
        if self.coordinate[0] not in self.X_COORDINATE_STRING:
            raise ValueError

    def validate_y(self):
        """ Validates the Y value of the coordinate string """
        if self.coordinate[1] not in self.Y_COORDINATE_STRING:
            raise ValueError

    def get_x(self):
        """ Returns X position of Coordinate """
        return self.X_COORDINATE_STRING.index(self.coordinate[0])

    def get_y(self):
        """ Returns Y position of Coordinate """
        return self.Y_COORDINATE_STRING.index(self.coordinate[1])

    def __eq__(self, other):
        return other.coordinate == self.coordinate

    def __lt__(self, other):
        return self.coordinate < other.coordinate

    def __hash__(self):
        return hash(self.coordinate)

    def __repr__(self):
        return self.coordinate

    @classmethod
    def parse_int_pair(cls, x_coord, y_coord):
        """ Returns Coordinates or None"""
        if not cls.is_int_pair_valid(x_coord, y_coord):
            return None

        x_str = cls.X_COORDINATE_STRING[x_coord]
        y_str = cls.Y_COORDINATE_STRING[y_coord]
        coordinates_string = x_str + y_str
        return Coordinates(coordinates_string)

    @classmethod
    def is_int_pair_valid(cls, x_coord, y_coord):
        """ Returns true if x,y is on board or false """
        if x_coord < 0 or x_coord > 7:
            return False

        if y_coord < 0 or y_coord > 7:
            return False

        return True
