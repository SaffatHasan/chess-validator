""" Contains the Piece class and validation logic """
from src.main.python.exceptions.board_exceptions import InvalidPieceException
from src.main.python.state.piece_enums import PieceTypes, Color
from src.main.python.motion import board_movement
from src.main.python.util import compose


class Piece:
    """ Functional unit of board """

    def __init__(self, piece_type, color):
        """ Initializes a piece with type and color """
        self.piece_type = piece_type
        self.color = color
        self.validate()
        self.moves = []
        self.attack_moves = []
        self.has_multi_moves = False

    def validate(self):
        """ Validates that a piece has been initialized correctly """
        self.validate_type()
        self.validate_color()

    def validate_type(self):
        """ Validates the type of a piece """
        if not isinstance(self.piece_type, PieceTypes):
            raise InvalidPieceException

    def validate_color(self):
        """ Validates the color of a piece """
        if not isinstance(self.color, Color):
            raise InvalidPieceException

    def get_type(self):
        """ Returns the type for a piece """
        return self.piece_type

    def get_color(self):
        """ Returns the color for a piece """
        return self.color

    def get_regular_moves(self):
        """ Returns basic movement functions associated with the piece """
        return self.moves

    def get_attack_moves(self):
        """ Returns the attack-style moves associated with the piece """
        return self.attack_moves

    def __eq__(self, other):
        return other.get_type() == self.get_type() and other.get_color() == self.get_color()

    def __repr__(self):
        return str(self.get_color()) + str(self.get_type())


class Pawn(Piece):
    """ Pawn """

    def __init__(self, color):
        super().__init__(PieceTypes.PAWN, color)
        forward = board_movement.get_forward_by_color(color)
        left = board_movement.get_left_by_color(color)
        right = board_movement.get_right_by_color(color)
        self.moves = [forward]
        self.attack_moves = [compose(forward, left), compose(forward, right)]


class Bishop(Piece):
    """ Bishop """

    def __init__(self, color):
        super().__init__(PieceTypes.BISHOP, color)
        self.moves = board_movement.get_diagonal_movements()
        self.attack_moves = self.moves
        self.has_multi_moves = True


class Queen(Piece):
    """ Queen """

    def __init__(self, color):
        super().__init__(PieceTypes.QUEEN, color)
        self.moves = board_movement.get_diagonal_movements(
        ) | board_movement.get_straight_movements()
        self.attack_moves = self.moves
        self.has_multi_moves = True


class Rook(Piece):
    """ Rook """

    def __init__(self, color):
        super().__init__(PieceTypes.ROOK, color)
        self.moves = board_movement.get_straight_movements()
        self.attack_moves = self.moves
        self.has_multi_moves = True


class Knight(Piece):
    """ Knight """

    def __init__(self, color):
        super().__init__(PieceTypes.KNIGHT, color)
        forward = board_movement.get_forward_by_color(color)
        backward = board_movement.get_backward_by_color(color)
        left = board_movement.get_left_by_color(color)
        right = board_movement.get_right_by_color(color)
        self.moves = [
            compose(forward, forward, left),
            compose(forward, left, left),
            compose(forward, forward, right),
            compose(forward, right, right),
            compose(backward, backward, left),
            compose(backward, left, left),
            compose(backward, backward, right),
            compose(backward, right, right),
        ]
        self.attack_moves = self.moves


class King(Piece):
    """ King """

    def __init__(self, color):
        super().__init__(PieceTypes.KING, color)
        self.moves = board_movement.get_straight_movements(
        ) | board_movement.get_diagonal_movements()
        self.attack_moves = self.moves
