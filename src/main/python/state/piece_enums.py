""" Enums for piece attributes """
import enum


class PieceTypes(enum.Enum):
    """ Singleton constants for piece types """
    KING = "K"
    QUEEN = "Q"
    KNIGHT = "N"
    ROOK = "R"
    BISHOP = "B"
    PAWN = "P"

    def __str__(self):
        return self.value


class Color(enum.Enum):
    """ Singleton constants for player colors """
    WHITE = "W"
    BLACK = "B"

    def __str__(self):
        return self.value
