""" Contains the Board class """


class Board:
    """ Chess Board """

    def __init__(self):
        self.board_objects = {}

    def get_piece_at_coordinate(self, coordinates):
        """ Returns piece at a given coordinate """
        if coordinates not in self.board_objects:
            return None
        return self.board_objects[coordinates]

    def remove_piece_at_coordinate(self, coordinate):
        """ Removes a piece at specified coordinate """
        if coordinate in self.board_objects:
            del self.board_objects[coordinate]

    def add_piece_to_board(self, piece, coordinate):
        """ Adds a piece to a board at specified coordinate """
        self.board_objects[coordinate] = piece

    def get_board_as_dict(self):
        """ Returns a dictionary of the board as a key/val
        set of coordinates to pieces """
        return self.board_objects
