""" File: util.py

    Contains utility functions
"""
import functools


def compose(*functions):
    """ Function that returns a composition of functions passed in """
    return functools.reduce(
        lambda f, g: lambda x: f(
            g(x)), functions, lambda x: x)


def compose_n_times(function, num_iter):
    """ Returns a function composed on itself n times """
    resulting_function = function
    for _ in range(1, num_iter):
        resulting_function = compose(function, resulting_function)

    return resulting_function
