""" Contains logic for getting user io

    This module gets the board state and then
    asks which piece to move
"""
import os


class AbstractInput:
    """ Abstract base class for io objects """

    def get_white_pieces_input(self):
        """ Returns the items on the first line """
        raise NotImplementedError

    def get_black_pieces_input(self):
        """ Returns the items on the second line """
        raise NotImplementedError

    def get_piece_to_move(self):
        """ Returns the item on the third line """
        raise NotImplementedError


class ConsoleInput(AbstractInput):
    """ Console Input class """

    def __init__(self, input_wrapper):
        """ Initialization """
        self.input_wrapper = input_wrapper

    def get_white_pieces_input(self):
        """ Gets user io for board state of white pieces """
        return self.input_wrapper.input("WHITE: ")

    def get_black_pieces_input(self):
        """ Gets user io for board state of white pieces """
        return self.input_wrapper.input("BLACK: ")

    def get_piece_to_move(self):
        """ Gets user specification for what piece to move """
        return self.input_wrapper.input("PIECE TO MOVE: ")


class FileInput(AbstractInput):
    """ Reads a file for the io of a chess state """
    WHITE_LINE = 0
    BLACK_LINE = 1
    MOVE_PIECE_LINE = 2

    def __init__(self, file_name):
        """ Initialize class with a file name (absolute path typically)
            Raises FileNotFound if specified item is not a file
        """
        self.file = file_name
        if not os.path.isfile(file_name):
            raise FileNotFoundError

    def get_white_pieces_input(self):
        """ Returns the items on the first line """
        return self.return_line_n(self.WHITE_LINE)

    def get_black_pieces_input(self):
        """ Returns the items on the second line """
        return self.return_line_n(self.BLACK_LINE)

    def get_piece_to_move(self):
        """ Returns the item on the third line """
        return self.return_line_n(self.MOVE_PIECE_LINE)

    def return_line_n(self, line_index):
        """ Returns the Nth line (index starting at 0) """
        with open(self.file) as file_handle:
            self.discard_lines(file_handle, line_index)
            return file_handle.readline().strip()

    @staticmethod
    def discard_lines(file_handle, num_lines):
        """ Discards lines from 0 to num_lines """
        for _ in range(num_lines):
            file_handle.readline()
