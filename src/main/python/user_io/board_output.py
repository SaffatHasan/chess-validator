""" Contains logic for printing user print
"""


class ConsoleOutput:
    """ Console Output class """

    def __init__(self, output_wrapper):
        """ Initialization """
        self.output_wrapper = output_wrapper

    def print_moves(self, piece_str, moves):
        """ Prints legal moves for given piece and given moves """
        moves_str = self.moves_to_string(moves)
        output_str = "LEGAL MOVES FOR {}: {}".format(piece_str, moves_str)
        self.output_wrapper.print(output_str)

    @staticmethod
    def moves_to_string(moves):
        """ Converts from set of coordinates that represent moves to a
            string of sorted moves with comma delimiter """
        return ", ".join(str(move) for move in sorted(moves))

    def print_error(self, error):
        """ Called when an invalid board state is specified """
        self.output_wrapper.print("Encountered error: {}".format(str(error)))
